﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_seminar3
{
    class Program
    {

        private static void numeC(string nume, string prenume, out string numeC)
        {
            numeC = $"{nume} {prenume}";
        }

        static void Main(string[] args)
        {

            Random rand = new Random();
            int nrAleator = rand.Next(100);
            // Console.WriteLine(nrAleator);
            int nr;
            bool gasit = false;
            int incercari = 1;
            int dif = 0;
            while (!gasit)
            {
                Console.Write("Introduceti nr: ");
                while (int.TryParse(Console.ReadLine(), out nr) == false || nr > 100)
                {
                    Console.WriteLine("Nr introdus nu este valid");
                    Console.Write("Introduceti alt nr: ");
                }
                if (nr == nrAleator)
                {
                    gasit = true;
                    Console.WriteLine("\nFelicitari! Nr este: " + nr + "\nNr de incercari este: " + incercari);
                }
                else
                {
                    incercari++;
                    if (nr < nrAleator)
                        dif = nrAleator - nr;
                    else
                        dif = nr - nrAleator;
                    if (dif <= 3)
                        Console.WriteLine("Foarte fierbinte");
                    else
                        if (dif <= 5)
                        Console.WriteLine("Fierbinte");
                    else
                        if (dif <= 10)
                        Console.WriteLine("Cald");
                    else
                        if (dif <= 20)
                        Console.WriteLine("Caldut");
                    else
                        if (dif <= 50)
                        Console.WriteLine("Rece");
                    else
                        if (dif > 50)
                        Console.WriteLine("Foarte rece");
                }

            }
            Console.ReadKey();
        }
    }
}


